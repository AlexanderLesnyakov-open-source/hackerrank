﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class AVeryBigSumTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string arraySize = "5";
            const string elements = "1000000001 1000000002 1000000003 1000000004 1000000005";
            Assert.AreEqual("5000000015", AVeryBigSum.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionZeroArraySize()
        {
            const string arraySize = "0";
            const string elements = "";
            Assert.AreEqual(string.Empty, AVeryBigSum.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string arraySize = "";
            const string elements = "";
            Assert.AreEqual(string.Empty, AVeryBigSum.Solution(arraySize, elements));
        }
    }
}