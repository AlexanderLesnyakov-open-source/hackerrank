﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class PlusMinusTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string arraySize = "6";
            const string elements = "-4 3 -9 0 4 1";
            const string expectedResult =
@"0.500000
0.333333
0.166667";
            Assert.AreEqual(expectedResult, PlusMinus.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArray()
        {
            const string arraySize = "0";
            const string elements = "";
            Assert.AreEqual(string.Empty, PlusMinus.Solution(arraySize, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string arraySize = "";
            const string elements = "";
            Assert.AreEqual(string.Empty, PlusMinus.Solution(arraySize, elements));
        }
    }
}