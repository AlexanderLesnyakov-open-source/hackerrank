﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class StaircaseTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string staircaseHeight = "6";
            const string expectedResult =
                @"     #
    ##
   ###
  ####
 #####
######";
            Assert.AreEqual(expectedResult, Staircase.Solution(staircaseHeight));
        }

        [TestMethod]
        public void TestSolutionZeroHeight()
        {
            const string staircaseHeight = "0";
            Assert.AreEqual(string.Empty, Staircase.Solution(staircaseHeight));
        }

        [TestMethod]
        public void TestSolutionEmptyHeight()
        {
            const string staircaseHeight = "";
            Assert.AreEqual(string.Empty, Staircase.Solution(staircaseHeight));
        }
    }
}