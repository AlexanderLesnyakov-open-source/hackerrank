﻿using Hackerrank.Algorithms.Warmup;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hackerrank.Test.Algorithms.Warmup
{
    [TestClass]
    public class DiagonalDifferenceTest
    {
        [TestMethod]
        public void TestSolution()
        {
            const string size = "3";
            const string elements = @"11 2 4
4 5 6
10 8 -12";
            Assert.AreEqual("15", DiagonalDifference.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionZeroArraySize()
        {
            const string size = "0";
            const string elements = "";
            Assert.AreEqual(string.Empty, DiagonalDifference.Solution(size, elements));
        }

        [TestMethod]
        public void TestSolutionEmptyArraySize()
        {
            const string size = "";
            const string elements = "";
            Assert.AreEqual(string.Empty, DiagonalDifference.Solution(size, elements));
        }
    }
}