﻿using System;
using Hackerrank.Extensions;

namespace Hackerrank.Algorithms.Implementation
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/cavity-map
    /// </summary>
    public static class CavityMap
    {
        public static string Solution(string size, string elements)
        {
            if (string.IsNullOrWhiteSpace(size))
                return string.Empty;

            var n = Convert.ToInt32(size);
            if (n == 0)
                return string.Empty;
            
            var grid = new string[n];
            var rows = elements.SplitByNewLine();
            for (var gridI = 0; gridI < n; gridI++)
            {
                grid[gridI] = rows[gridI];
            }

            var result = string.Empty;

            for (var i = 0; i < grid.Length; i++)
            {
                for (var j = 0; j < grid[i].Length; j++)
                {
                    if (i > 0 && j > 0 && i < grid.Length - 1 && j < grid.Length - 1)
                    {
                        var depth = char.GetNumericValue(grid[i][j]);
                        if (char.GetNumericValue(grid[i - 1][j]) < depth && //up
                            char.GetNumericValue(grid[i][j - 1]) < depth && //left
                            char.GetNumericValue(grid[i][j + 1]) < depth && //right
                            char.GetNumericValue(grid[i + 1][j]) < depth) //bottom
                        {
                            result += "X";
                            continue;
                        }
                    }
                    result += grid[i][j];
                }
                if (i < grid.Length - 1)
                    result += Environment.NewLine;
            }

            return result;
        }
    }
}