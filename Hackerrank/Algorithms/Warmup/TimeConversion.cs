﻿using System;
using System.Globalization;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/time-conversion
    /// </summary>
    public static class TimeConversion
    {
        public static string Solution(string time)
        {
            if (string.IsNullOrWhiteSpace(time))
                return string.Empty;

            var dateTime = DateTime.ParseExact(time, "hh:mm:sstt", CultureInfo.InvariantCulture);
            return dateTime.ToString("T", CultureInfo.InvariantCulture);
        }
    }
}