﻿using System;
using System.Text;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/staircase
    /// </summary>
    public static class Staircase
    {
        public static string Solution(string staircaseHeight)
        {
            if (string.IsNullOrWhiteSpace(staircaseHeight))
                return string.Empty;

            var n = Convert.ToInt32(staircaseHeight);

            if (n == 0)
                return string.Empty;

            var builder = new StringBuilder();
            builder.Append(' ', n);
            var result = string.Empty;
            for (var i = n - 1; i >= 0; i--)
            {
                builder[i] = '#';
                result += builder + (i == 0 ? "" : Environment.NewLine);
            }

            return result;
        }
    }
}