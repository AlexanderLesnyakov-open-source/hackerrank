﻿using System;
using System.Linq;

namespace Hackerrank.Algorithms.Warmup
{
    /// <summary>
    ///     Solution for https://www.hackerrank.com/challenges/a-very-big-sum
    /// </summary>
    public static class AVeryBigSum
    {
        public static string Solution(string arraySize, string elements)
        {
            if (string.IsNullOrWhiteSpace(arraySize))
                return string.Empty;

            var size = Convert.ToInt32(arraySize);
            if (size == 0)
                return string.Empty;

            var arrTemp = elements.Split(' ');
            var arr = Array.ConvertAll(arrTemp, int.Parse);
            var result = arr.Aggregate<int, long>(0, (current, i) => current + i);
            return result.ToString();
        }
    }
}